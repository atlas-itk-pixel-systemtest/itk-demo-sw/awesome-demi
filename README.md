<div align="center">

# Awesome List Template [![Awesome](https://awesome.re/badge.svg)](https://awesome.re) [![lint](https://github.com/YOUR_GITHUB_USER/YOUR_REPO/actions/workflows/lint.yaml/badge.svg)](https://github.com/YOUR_GITHUB_USER/YOUR_REPO/actions/workflows/lint.yaml)

A template for an awesome list with required conventions out of the box!

(put ATLAS ITk logo here)


Awesome List of all useful references for Detector Microservices.

</div>

## Contents

- [Microservices)](#microservices)
- [FELIX](#felix)
- [DAQ](#daq)

## Microservices

- [microservices.io](https://microservices.io/) - microservices.io Website.

## FELIX

- [FELIX](https://atlas-project-felix.web.cern.ch/atlas-project-felix/) - The ATLAS FELIX Project Website.

## DAQ

- [YARR](https://gitlab.cern.ch/YARR) - Yet Another Rapid Readout.
- [itk-felix-sw](https://gitlab.cern.ch/itk-felix-sw) - TDAQ integrated readout.
- [Project Orion](https://orion.docs.cern.ch/) - high performance readout system.


## Follow

Who else should we be following!? 
(maybe put similar efforts in other experiments like DUNE, CMS etc. here)

## Contributing

[Contributions of any kind welcome, just follow the guidelines](contributing.md)!

### Contributors

gbrandt
(put your name here if you added a link)
